# Función directories

## Instalación via composer

```bin
composer require tinyphp-function/directories
```

## Funciones

```php
/** Retorna los archivos del directorio y sus subdirectorios en una sola lista */
function files_in_dir(string $directory, bool $include_hiddens = false, int $level = 1): array


/** Retorna los directorios y subdirectorios de una ruta específica */
function dirs_in_dir(string $directory, int $level = 1): array


/** Elimina una ruta por completo */
function unlink_dir(string $dir): bool
```
