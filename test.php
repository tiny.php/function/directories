<?php
/**
 * test.php
 * /
 * 
 * @package tinyphp-function/directories
 * @author Jesús Rojas <jrojash@jys.pe>
 * @copyright 2024, J&S Perú <https://jys.pe>
 * @created 2024-11-15 19:18:56
 * @version 20241115192655 (Rev. 254)
 * @license MIT
 * @filesource
 * 
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

require_once 'vendor/autoload.php';

$basedir = 'pruebas';
$dir     = "$basedir/sub/directory";

if (!file_exists($dir))
    mkdir($dir, 0777, true);

file_put_contents("$dir/archivo.txt", time());

// rmdir($basedir); ## PHP Warning:  rmdir($basedir): Directory not empty

unlink_dir($basedir); ## success