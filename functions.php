<?php
/**
 * functions.php
 * /
 * 
 * @package tinyphp-function/directories
 * @author Jesús Rojas <jrojash@jys.pe>
 * @copyright 2024, J&S Perú <https://jys.pe>
 * @created 2024-11-15 19:16:08
 * @version 20241115192634 (Rev. 153)
 * @license MIT
 * @filesource
 * 
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

if (!function_exists('files_in_dir'))
{
    /**
     * Retorna los archivos del directorio y sus subdirectorios en una sola lista
     *
     * @param string $directory
     * @param boolean $include_hiddens
     * @param integer $level
     * @return array
     */
    function files_in_dir(string $directory, bool $include_hiddens = false, int $level = 1): array
    {
        if (!file_exists($directory))
            return [];

        $lista = scandir($directory);

        if ($lista === false)
            return [];

        $arr = [];
        foreach ($lista as $file)
        {
            if (in_array($file, ['.', '..']))
                continue; ## omitir directorios padre

            if (!$include_hiddens and ($file === 'index.html' or $file[0] === '.'))
                continue; ## omitir ocultos

            $ruta = "$directory/$file";

            if (!is_dir($ruta))
            {
                $arr[] = $ruta;
                continue;
            }

            $arr = array_merge($arr, files_in_dir($ruta, $level + 1, $include_hiddens));
        }

        return array_unique($arr);
    }
}

if (!function_exists('dirs_in_dir'))
{
    /**
     * Retorna los directorios y subdirectorios de una ruta específica
     *
     * @param string $directory
     * @param integer $level
     * @return array
     */
    function dirs_in_dir(string $directory, int $level = 1): array
    {
        if (!file_exists($directory))
            return [];

        $lista = scandir($directory);

        if ($lista === false)
            return [];

        $arr = [];
        foreach ($lista as $file)
        {
            if (in_array($file, ['.', '..']))
                continue; ## omitir directorios padre

            $ruta = "$directory/$file";
            if (!is_dir($ruta))
                continue;

            $arr[] = $ruta;

            $arr = array_merge($arr, dirs_in_dir($ruta, $level + 1));
        }

        return array_unique($arr);
    }
}

if (!function_exists('unlink_dir'))
{
    /**
     * Elimina una ruta por completo
     *
     * @param string $dir
     * @return boolean
     */
    function unlink_dir(string $dir): bool
    {
        if (!file_exists($dir))
            return true;

        if (!is_dir($dir))
        {
            if (file_exists($dir))
                unlink($dir);
            return true;
        }

        $files = files_in_dir($dir, true);
        foreach ($files as $file)
        {
            if (file_exists($file))
                unlink($file);
        }

        $dirs = dirs_in_dir($dir, true);
        $dirs = array_reverse($dirs); ## ordenar los subdirectorios primero

        foreach ($dirs as $subdir)
        {
            if (file_exists($subdir))
                rmdir($subdir);
        }

        return rmdir($dir);
    }
}